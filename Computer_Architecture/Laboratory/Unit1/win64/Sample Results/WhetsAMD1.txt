 Athlon 64 X2 Dual Core 4200+ 2.21 GHz, Asus 
 A8N-SLI Deluxe, 1 GB DCDDR RAM, WinXP Pro X64

 Whets32SSE - 32 bits
 
 ********************************************************

 Whetstone Single Precision SSE Benchmark Mon Aug 08 11:39:47 2005

 Via Microsoft 32-bit C/C++ Optimizing Compiler Version 13.10.3077 for 80x86

 MFLOPS    Vax  MWIPS MFLOPS MFLOPS MFLOPS    Cos    Exp  Fixpt     If  Equal
  Gmean   MIPS            1      2      3    MOPS   MOPS   MOPS   MOPS   MOPS
    583  12197   2313    655    656    461   51.0   36.3   1988   2210   3305

 Numeric results were as expected

  CPUID and RDTSC Assembly Code
  CPU AuthenticAMD, Features Code 178BFBFF, Model Code 00020FB1
  AMD Athlon(tm) 64 X2 Dual Core Processor 4200+ Measured 2211 MHz
  Has MMX, Has SSE, Has SSE2, Has SSE3, Has 3DNow, 
  Windows GetSystemInfo, GetVersionEx, GlobalMemoryStatus
  Intel processor architecture, 2 CPUs 
  Windows NT  Version 5.2, build 3790, Service Pack 1
  Memory 1024 MB, Free 649 MB
  User Virtual Space 4096 MB, Free 4085 MB

